package com.example.android.myapplication

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import android.widget.GridView
import android.widget.ImageView
import android.widget.Toast
import java.lang.Math.abs
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    var picture:  Bitmap? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        var imageviewPicture: ImageView
        val gridview: GridView = findViewById(R.id.galary_gridview_id)
        imageviewPicture = findViewById(R.id.activity_picture)
        gridview.adapter = GallaryImageAdapter(this)
        lateinit var noisyBitmap: Bitmap

        val opts = BitmapFactory.Options()
        opts.inMutable = true
        var flag: Boolean = true

        gridview.onItemClickListener =
                AdapterView.OnItemClickListener { parent, v, position, id ->
                    Toast.makeText(this, "$position", Toast.LENGTH_SHORT).show()
                    Log.i("Picture", mThumbIds.get(position).toString())
                    //imageviewPicture.setImageResource(mThumbIds.get(position))
                    //imageviewPicture.setImageResource(R.drawable.sample_3)

                    val bitmap = BitmapFactory.decodeResource(resources, mThumbIds.get(position), opts)
                    //var mutableBitmap = decodeResource(resources, mThumbIds.get(position))
                    //bitmap = BitmapFactory.Options.inMutable()
                    Log.i("Bitmap height", bitmap.height.toString())
                    Log.i("Bitmap width", bitmap.width.toString())
                    Log.i("Bitmap getpixel() color", bitmap.getPixel(200, 200).toString())
                    Log.i("ColorRed getpixel", Color.red(bitmap.getPixel(200, 200)).toString())
                    //mutableBitmap = BitmapFactory.Options.decodeResource(resources, mThumbIds.get(position))
                    //mutableBitmap.inMutable = true
                    noisyBitmap = Bitmap.createBitmap(bitmap.width,bitmap.height,Bitmap.Config.RGB_565)
                    noisyBitmap = bitmap
                    Log.i("ColorRed getpixel", Color.red(noisyBitmap.getPixel(200, 200)).toString())
                    imageviewPicture.setImageBitmap(bitmap)
                    imageviewPicture.setEnabled(true)
                    flag = true
                }

        var redwindow: ArrayList<Int> = ArrayList()
        var greenwindow: ArrayList<Int> = ArrayList()
        var bluewindow: ArrayList<Int> = ArrayList()



        var recievedred: Int = 0
        var recievedblue: Int = 0
        var recievedgreen: Int = 0

        imageviewPicture.setOnClickListener {
            Log.i("sdsdsdsdsdsdsdsdsd", noisyBitmap.getPixel(0,0).toString())

            for(i in 0..noisyBitmap.width){
                redwindow.add(i, 0)
                greenwindow.add(i, 0)
                bluewindow.add(i, 0)
            }

            if(flag == false){

                var porog: Int = 150
                var x: Int = 0
                for (i in 0..(noisyBitmap.width - 1) ) {
                    for(j in 2..(noisyBitmap.height-3)){
                        recievedred = Color.red(noisyBitmap.getPixel(i,j))
                        recievedblue = Color.blue(noisyBitmap.getPixel(i,j))
                        recievedgreen = Color.green(noisyBitmap.getPixel(i,j))
                        redwindow.set(i,(Color.red(noisyBitmap.getPixel(i,(j-2)))
                                + Color.red(noisyBitmap.getPixel(i,(j-1)))
                                + Color.red(noisyBitmap.getPixel(i,(j+1)))
                                + Color.red(noisyBitmap.getPixel(i,(j+2))))/4)
                        redwindow.set(i, (Color.blue(noisyBitmap.getPixel(i,(j-2)))
                                + Color.blue(noisyBitmap.getPixel(i,(j-1)))
                                + Color.blue(noisyBitmap.getPixel(i,(j+1)))
                                + Color.blue(noisyBitmap.getPixel(i,(j+2))))/4)
                        redwindow.set(i, (Color.green(noisyBitmap.getPixel(i,(j-2)))
                                + Color.green(noisyBitmap.getPixel(i,(j-1)))
                                + Color.green(noisyBitmap.getPixel(i,(j+1)))
                                + Color.green(noisyBitmap.getPixel(i,(j+2))))/4)

                        if(abs(recievedred - redwindow[i]) > porog) {
                            recievedred = redwindow[i]
                            recievedblue = bluewindow[i]
                            recievedgreen = greenwindow[i]
                            var valera = Color.rgb(recievedred, recievedgreen, recievedblue)
                            noisyBitmap.setPixel(i, j, valera)
                        }
                        //recievedcolor = noisyBitmap.getPixel(i,j)
                    }
                }
                Toast.makeText(applicationContext, "Otrabotalo", Toast.LENGTH_SHORT).show()
                imageviewPicture.setImageBitmap(noisyBitmap)
                imageviewPicture.setEnabled(false)
            }

            if(flag) {
                var random = Random()

                for (i in 0..15000) {
                    noisyBitmap.setPixel(random.nextInt(noisyBitmap.width),
                            random.nextInt(noisyBitmap.height), random.nextInt(16_500_000))
                }

                imageviewPicture.setImageBitmap(noisyBitmap)
                Log.i("slsldskdkskdksdkskds", "sdkskdkdkskdskdkskdkskdksdkskdkskdkskdkssdsds")
                flag = false
            }
        }
    }
}
